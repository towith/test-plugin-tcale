#include "stdafx.h"
#include "CppUnitTest.h"
#include "StdAfx.h"
#include <iostream>
#include "TCalcFuncSets.h"
#include <ctime>
#include <tchar.h>

#undef  UNICODE
#undef  _UNICODE

using namespace std;
using namespace Microsoft::VisualStudio::CppUnitTestFramework;


namespace test {

	// using namespace std;
	std::string ShowDateTime(const tm& t, const string& format)
	{
		char s[100];
		strftime(s, sizeof(s), format.c_str(), &t);
		return string(s);
	}

	std::string ShowDateTime(const time_t& t, const string& format)
	{
		tm* _tm = gmtime(&t);
		return ShowDateTime(*_tm, format);
	}

	time_t now = time(0);
	std::string ShowYMD(const time_t& t, const char& dateDiv = '-')
	{

		//std::ostringstream format = ;
		//format << "%Y-" << dateDiv << "%m-" << dateDiv << "%d" << " %H:%M:%s";
		//string fmt = format.str();
		return ShowDateTime(t, "%Y-%m-%d %H:%M:%s");
	}

}



namespace MyTest
{
	auto testPad(std::string code) {
		int len = code.length();
		if (len < 6) {
			code.insert(code.begin(), 6 - len, '0');
		}
		return code;
	}


	TEST_CLASS(MyTests)
	{
	public:
		TEST_METHOD(MyTestMethod)
		{

			time_t now = time(0);
			//cout << "TEST";
			//cout << timeS << " haha";

			string out = testPad("abc");
			//Logger::WriteMessage(out);
			int	len = out.length();
			Assert::AreEqual(1, len);
		}
	};
}