﻿#include "stdafx.h"
#include "TCalcFuncSets.h"
#include <fstream>
#include <time.h>
#include <string>
#include <sys/stat.h>
#include <tchar.h>
#include <iomanip>
#include <windows.h>
#include <thread>
#include <iostream>
#include <cstdio>
#include <map> 
std::map<std::string, bool> waitMap;

#ifdef _DEBUG 
#pragma comment(lib, "json/jsoncpp.lib") 
#else 
#pragma comment(lib, "json/jsoncpp.lib") 
#endif 
#include "json/json.h"



using namespace std;
std::ofstream dataFile;
std::ofstream logF;
bool blockInfoInited = false;
Json::Value blkInfoRoot;
bool watchFileThreadInited = false;
int CurrentFunType = 0;

//生成的dll及相关依赖dll请拷贝到通达信安装目录的T0002/dlls/下面,再在公式管理器进行绑定

std::ofstream* getLogF() {
	if (!logF.is_open()) {
		logF.open("N:\\tdx\\log.txt", ios_base::app);
	}
	return &logF;
}

void dummyDataConstant(int DataLen, float* pfOUT, float value) {
	//for (int i = 0;i < DataLen;i++) {
	//	//*getLogF() << "write " << i << ":" << value << endl;
	//	pfOUT[i] = value;
	//}
	pfOUT[0] = value;
	pfOUT[DataLen - 1] = value;
}
void dummyData(int DataLen, float* pfOUT) {
	dummyDataConstant(DataLen, pfOUT, (float)-999999);
}

void dummyDataTrue(int DataLen, float* pfOUT) {
	dummyDataConstant(DataLen, pfOUT, (float)1);
}
void dummyDataFalse(int DataLen, float* pfOUT) {
	dummyDataConstant(DataLen, pfOUT, (float)0);
}

void sameData(int DataLen, float* pfOUT, float data) {
	for (int i = 0;i < DataLen;i++)
		pfOUT[i] = data;
}


inline bool exists_path(const std::string& name) {
	struct stat buffer;
	return (stat(name.c_str(), &buffer) == 0);
}


void checkFileSig(string sigName) {
	while (!waitMap[sigName]) {
		string path = "N:\\tdx\\FileSig\\" + sigName;
		if ((exists_path(path))) {
			*getLogF() << "got " << sigName << endl;
			waitMap[sigName] = true;
		}
		else {
			*getLogF() << "wait " << sigName << endl;
			Sleep(1000);
		}
	}
}

std::string get_file_contents(const char* filename)
{
	std::ifstream infile(filename, std::ios::in | std::ios::binary);
	if (infile.is_open()) {
		std::string contents;
		infile.seekg(0, std::ios::end);
		contents.resize(infile.tellg()); // same as: std::string contents(infile.tellg(), '\0');
		infile.seekg(0, std::ios::beg);
		infile.read(&contents[0], contents.size());
		infile.close();
		return(contents);
	}
	throw(errno);
}

void waitFilePresent1(std::ofstream* logF)
{
	DWORD cbBytes;
	char file_name[MAX_PATH]; //设置文件名;
	char file_rename[MAX_PATH]; //设置文件重命名后的名字;
	char notify[1024];
	int count = 0; //文件数量。可能同时拷贝、删除多个文件，可以进行更友好的提示;
	TCHAR* dir = _T("N:\\tdx\\FileSig1\\");


	HANDLE dirHandle = CreateFile(dir,
		GENERIC_READ | GENERIC_WRITE | FILE_LIST_DIRECTORY,
		FILE_SHARE_READ | FILE_SHARE_WRITE,
		NULL,
		OPEN_EXISTING,
		FILE_FLAG_BACKUP_SEMANTICS,
		NULL);

	if (dirHandle == INVALID_HANDLE_VALUE) //若网络重定向或目标文件系统不支持该操作，函数失败，同时调用GetLastError()返回ERROR_INVALID_FUNCTION
	{
		*logF << "error" + GetLastError() << endl;
	}

	// memset(notify, 0, strlen(notify));
	memset(notify, 0, 1024);
	FILE_NOTIFY_INFORMATION* pnotify = (FILE_NOTIFY_INFORMATION*)notify;

	*logF << "Start Monitor1..." << endl;

	while (true)
	{
		if (ReadDirectoryChangesW(dirHandle, &notify, 1024, true,
			FILE_NOTIFY_CHANGE_FILE_NAME | FILE_NOTIFY_CHANGE_DIR_NAME | FILE_NOTIFY_CHANGE_SIZE,
			&cbBytes, NULL, NULL))
		{
			//转换文件名为多字节字符串;
			if (pnotify->FileName)
			{
				// memset(file_name, 0, strlen(file_name));
				memset(file_name, 0, MAX_PATH);
				WideCharToMultiByte(CP_ACP, 0, pnotify->FileName, pnotify->FileNameLength / 2, file_name, 99, NULL, NULL);
			}

			//获取重命名的文件名;
			if (pnotify->NextEntryOffset != 0 && (pnotify->FileNameLength > 0 && pnotify->FileNameLength < MAX_PATH))
			{
				PFILE_NOTIFY_INFORMATION p = (PFILE_NOTIFY_INFORMATION)((char*)pnotify + pnotify->NextEntryOffset);
				// memset(file_rename, 0, sizeof(file_rename));
				memset(file_rename, 0, MAX_PATH);
				WideCharToMultiByte(CP_ACP, 0, p->FileName, p->FileNameLength / 2, file_rename, 99, NULL, NULL);
			}

			bool present = false;
			//设置类型过滤器,监听文件创建、更改、删除、重命名等;
			switch (pnotify->Action)
			{
			case FILE_ACTION_ADDED:
				count++;
				//cout << "count  = " << count << endl;
				//cout << setw(5) << "file add:" << setw(5) << file_name << endl;
				*logF << "count  = " << count << endl;
				*logF << setw(5) << "file add:" << setw(5) << file_name << endl;
				if ("1.waitSig" == file_name) {
					present = true;
				}
				break;

			case FILE_ACTION_MODIFIED:
				cout << "file modified:" << setw(5) << file_name << endl;
				break;

			case FILE_ACTION_REMOVED:
				count++;
				cout << count << setw(5) << "file removed:" << setw(5) << file_name << endl;
				break;

			case FILE_ACTION_RENAMED_OLD_NAME:
				cout << "file renamed:" << setw(5) << file_name << "->" << file_rename << endl;
				break;

			default:
				//cout << "UNknow command!" << endl;
				*logF << "UNknow command!" << endl;
			}

			if (present) {
				*logF << "sig present:" << file_name << endl;
				break;
			}
		}
	}

	CloseHandle(dirHandle);
}
void waitFilePresent2(std::ofstream* logF)
{
	DWORD cbBytes;
	char file_name[MAX_PATH]; //设置文件名;
	char file_rename[MAX_PATH]; //设置文件重命名后的名字;
	char notify[1024];
	int count = 0; //文件数量。可能同时拷贝、删除多个文件，可以进行更友好的提示;
	TCHAR* dir = _T("N:\\tdx\\FileSig2\\");


	HANDLE dirHandle = CreateFile(dir,
		GENERIC_READ | GENERIC_WRITE | FILE_LIST_DIRECTORY,
		FILE_SHARE_READ | FILE_SHARE_WRITE,
		NULL,
		OPEN_EXISTING,
		FILE_FLAG_BACKUP_SEMANTICS,
		NULL);

	if (dirHandle == INVALID_HANDLE_VALUE) //若网络重定向或目标文件系统不支持该操作，函数失败，同时调用GetLastError()返回ERROR_INVALID_FUNCTION
	{
		*logF << "error" + GetLastError() << endl;
	}

	// memset(notify, 0, strlen(notify));
	memset(notify, 0, 1024);
	FILE_NOTIFY_INFORMATION* pnotify = (FILE_NOTIFY_INFORMATION*)notify;

	*logF << "Start Monitor2..." << endl;

	while (true)
	{
		if (ReadDirectoryChangesW(dirHandle, &notify, 1024, true,
			FILE_NOTIFY_CHANGE_FILE_NAME | FILE_NOTIFY_CHANGE_DIR_NAME | FILE_NOTIFY_CHANGE_SIZE,
			&cbBytes, NULL, NULL))
		{
			//转换文件名为多字节字符串;
			if (pnotify->FileName)
			{
				// memset(file_name, 0, strlen(file_name));
				memset(file_name, 0, MAX_PATH);
				WideCharToMultiByte(CP_ACP, 0, pnotify->FileName, pnotify->FileNameLength / 2, file_name, 99, NULL, NULL);
			}

			//获取重命名的文件名;
			if (pnotify->NextEntryOffset != 0 && (pnotify->FileNameLength > 0 && pnotify->FileNameLength < MAX_PATH))
			{
				PFILE_NOTIFY_INFORMATION p = (PFILE_NOTIFY_INFORMATION)((char*)pnotify + pnotify->NextEntryOffset);
				// memset(file_rename, 0, sizeof(file_rename));
				memset(file_rename, 0, MAX_PATH);
				WideCharToMultiByte(CP_ACP, 0, p->FileName, p->FileNameLength / 2, file_rename, 99, NULL, NULL);
			}

			bool present = false;
			//设置类型过滤器,监听文件创建、更改、删除、重命名等;
			switch (pnotify->Action)
			{
			case FILE_ACTION_ADDED:
				count++;
				//cout << "count  = " << count << endl;
				//cout << setw(5) << "file add:" << setw(5) << file_name << endl;
				*logF << "count  = " << count << endl;
				*logF << setw(5) << "file add:" << setw(5) << file_name << endl;
				if ("2.waitSig" == file_name) {
					present = true;
				}
				break;

			case FILE_ACTION_MODIFIED:
				cout << "file modified:" << setw(5) << file_name << endl;
				break;

			case FILE_ACTION_REMOVED:
				count++;
				cout << count << setw(5) << "file removed:" << setw(5) << file_name << endl;
				break;

			case FILE_ACTION_RENAMED_OLD_NAME:
				cout << "file renamed:" << setw(5) << file_name << "->" << file_rename << endl;
				break;

			default:
				//cout << "UNknow command!" << endl;
				*logF << "UNknow command!" << endl;
			}

			if (present) {
				*logF << "sig present:" << file_name << endl;
				break;
			}
		}
	}

	CloseHandle(dirHandle);
}
static void waitFilePresent(string fileNameSig, std::ofstream* logF)
{
	DWORD cbBytes;
	char file_name[MAX_PATH]; //设置文件名;
	char file_rename[MAX_PATH]; //设置文件重命名后的名字;
	char notify[1024];
	int count = 0; //文件数量。可能同时拷贝、删除多个文件，可以进行更友好的提示;
	TCHAR* dir = _T("N:\\tdx\\FileSig\\");


	HANDLE dirHandle = CreateFile(dir,
		GENERIC_READ | GENERIC_WRITE | FILE_LIST_DIRECTORY,
		FILE_SHARE_READ | FILE_SHARE_WRITE,
		NULL,
		OPEN_EXISTING,
		FILE_FLAG_BACKUP_SEMANTICS,
		NULL);

	if (dirHandle == INVALID_HANDLE_VALUE) //若网络重定向或目标文件系统不支持该操作，函数失败，同时调用GetLastError()返回ERROR_INVALID_FUNCTION
	{
		*logF << "error" + GetLastError() << endl;
	}

	// memset(notify, 0, strlen(notify));
	memset(notify, 0, 1024);
	FILE_NOTIFY_INFORMATION* pnotify = (FILE_NOTIFY_INFORMATION*)notify;

	*logF << "Start Monitor..." << endl;

	while (true)
	{
		if (ReadDirectoryChangesW(dirHandle, &notify, 1024, true,
			FILE_NOTIFY_CHANGE_FILE_NAME | FILE_NOTIFY_CHANGE_DIR_NAME | FILE_NOTIFY_CHANGE_SIZE,
			&cbBytes, NULL, NULL))
		{
			//转换文件名为多字节字符串;
			if (pnotify->FileName)
			{
				// memset(file_name, 0, strlen(file_name));
				memset(file_name, 0, MAX_PATH);
				WideCharToMultiByte(CP_ACP, 0, pnotify->FileName, pnotify->FileNameLength / 2, file_name, 99, NULL, NULL);
			}

			//获取重命名的文件名;
			if (pnotify->NextEntryOffset != 0 && (pnotify->FileNameLength > 0 && pnotify->FileNameLength < MAX_PATH))
			{
				PFILE_NOTIFY_INFORMATION p = (PFILE_NOTIFY_INFORMATION)((char*)pnotify + pnotify->NextEntryOffset);
				// memset(file_rename, 0, sizeof(file_rename));
				memset(file_rename, 0, MAX_PATH);
				WideCharToMultiByte(CP_ACP, 0, p->FileName, p->FileNameLength / 2, file_rename, 99, NULL, NULL);
			}

			bool present = false;
			//设置类型过滤器,监听文件创建、更改、删除、重命名等;
			switch (pnotify->Action)
			{
			case FILE_ACTION_ADDED:
				count++;
				//cout << "count  = " << count << endl;
				//cout << setw(5) << "file add:" << setw(5) << file_name << endl;
				*logF << "count  = " << count << endl;
				*logF << setw(5) << "file add:" << setw(5) << file_name << endl;
				if (fileNameSig == file_name) {
					present = true;
				}
				break;

			case FILE_ACTION_MODIFIED:
				cout << "file modified:" << setw(5) << file_name << endl;
				break;

			case FILE_ACTION_REMOVED:
				count++;
				cout << count << setw(5) << "file removed:" << setw(5) << file_name << endl;
				break;

			case FILE_ACTION_RENAMED_OLD_NAME:
				cout << "file renamed:" << setw(5) << file_name << "->" << file_rename << endl;
				break;

			default:
				//cout << "UNknow command!" << endl;
				*logF << "UNknow command!" << endl;
			}

			if (present) {
				*logF << "sig present:" << file_name << endl;
				break;
			}
		}
	}

	CloseHandle(dirHandle);
}



string getTimeStr() {
	time_t timep;
	time(&timep);
	char* fmt = "%Y-%m-%d %H:%M:%S";
	char tmp[sizeof(fmt) * 8];
	strftime(tmp, sizeof(tmp), fmt, localtime(&timep));
	string x = tmp;
	return x;
}

string getDateStr() {
	time_t timep;
	time(&timep);
	char* fmt = "%Y-%m-%d";
	char tmp[sizeof(fmt) * 8];
	strftime(tmp, sizeof(tmp), fmt, localtime(&timep));
	string x = tmp;
	return x;
}

auto padLeft(std::string code, int allLen) {
	int len = code.length();
	if (len < allLen) {
		code.insert(code.begin(), allLen - len, '0');
	}
	return code;
}

void outIfB(int DataLen, float* pfOUT, float* pfINa, float* pfINb, float* pfINc)
{

	std::ofstream outFile;
	string timeS = getTimeStr();

	if (*pfINb != 0) {
		//打开文件
		outFile.open("N:\\tdx\\tradeSigAsFile\\index.txt", ios_base::app);
		string code = std::to_string(((int)*pfINa));

		//写入数据
		outFile << timeS << " " << padLeft(code, 6) << "=" << *pfINb << "," << *pfINc << endl;
		//关闭文件
		outFile.close();
	}
	dummyData(DataLen, pfOUT);
}





void waitFileSig(int DataLen, float* pfOUT, float* pfINa, float* pfINb, float* pfINc)
{
	string sigName = // std::to_string(((int)*pfINa)) + "-" + 
		std::to_string(((int)*pfINb)) + "-" + std::to_string(((int)*pfINc)) + ".waitSig";
	//logF << "call waitFileSig ... " + sigName << endl;

	bool exist = waitMap[sigName];
	if (!exist) {
		*getLogF() << "wait ... " + sigName << endl;
		waitFilePresent(sigName, &logF);
		string path = "N:\\tdx\\FileSig\\" + sigName;
		if ((exists_path(path))) {
			waitMap[sigName] = true;
		}
	}

	dummyData(DataLen, pfOUT);
}

void blankFun(int DataLen, float* pfOUT, float* pfINa, float* pfINb, float* pfINc)
{

}
void waitFileSig1(int DataLen, float* pfOUT, float* pfINa, float* pfINb, float* pfINc)
{
	checkFileSig("1.waitSig");
	dummyData(DataLen, pfOUT);
}
void _waitFileSig1(int DataLen, float* pfOUT, float* pfINa, float* pfINb, float* pfINc)
{
	string sigName = // std::to_string(((int)*pfINa)) + "-" + 
		"1.waitSig";
	//logF << "call waitFileSig ... " + sigName << endl;

	bool exist = waitMap[sigName];
	if (!exist) {
		*getLogF() << "wait ... " + sigName << endl;
		waitFilePresent1(&logF);
		string path = "N:\\tdx\\FileSig\\" + sigName;
		if ((exists_path(path))) {
			waitMap[sigName] = true;
		}
	}

	dummyData(DataLen, pfOUT);
}

void log999(string code, string msg) {
	if (code == "999999") {
		*getLogF() << msg << endl;
	}
}
void GetCuttentFunType(int DataLen, float* pfOUT, float* pfINa, float* pfINb, float* pfINc) {
	string code = padLeft(std::to_string(((int)*pfINa)), 6);
	//log999(code, "GetCuttentFunType" + to_string(CurrentFunType));
	dummyDataConstant(DataLen, pfOUT, (float)CurrentFunType);
}

void waitFile(int DataLen, float* pfOUT, float* pfINa, float* pfINb, float* pfINc) {
	string code = padLeft(std::to_string(((int)*pfINa)), 6);
	if (code == "999999") {
		string sigName = "wait.Sig";
		bool exist = waitMap[sigName];
		if (!exist) {
			*getLogF() << "wait ... " + sigName << endl;
			waitFilePresent(sigName, &logF);
			string path = "N:\\tdx\\FileSig\\" + sigName;
			*getLogF() << "process:" << path << endl;
			if ((exists_path(path))) {
				waitMap[sigName] = true;
				*getLogF() << "get funType" << endl;
				FILE* f = fopen(path.c_str(), "r");
				int funType = fgetc(f) - '0';
				*getLogF() << "funType is " << funType << endl;
				CurrentFunType = funType;
				*getLogF() << "CurrentFunType is " << CurrentFunType << endl;
				fclose(f);

				//wchar_t funTypeS[1];
				//SetEnvironmentVariable("CurrentFunType","");
				//char* funTypeS = {};
				//DWORD c = GetEnvironmentVariable("CurrentFunType", funTypeS, 1);
			}
			else {
				*getLogF() << "no target file found" << endl;
			}
		};
	};
	dummyDataTrue(DataLen, pfOUT);
};
void CallScreen(int DataLen, float* pfOUT, float* pfINa, float* pfINb, float* pfINc) {
	string code = padLeft(std::to_string(((int)*pfINa)), 6);
	//log999(code, "CurrentFunType is " + to_string(CurrentFunType));
	if ((CurrentFunType % 2) == 1) {
		if (!code.find("880") == 0) {
			//			*getLogF() << "c1:" << code << endl;
			dummyDataTrue(DataLen, pfOUT);
			return;
		}
	}
	else {
		if (code.find("880") == 0) {
			//			*getLogF() << "c2:" << code << endl;
			dummyDataTrue(DataLen, pfOUT);
			return;
		}
	}
	dummyDataFalse(DataLen, pfOUT);
}


void testFun(int DataLen, float* pfOUT, float* pfINa, float* pfINb, float* pfINc) {
	*getLogF() << "enter test 1" << endl;
	std::thread::id this_id = std::this_thread::get_id();
	*getLogF() << "thread id:" << this_id << endl;
	dummyData(DataLen, pfOUT);
}

void waitFileSig2(int DataLen, float* pfOUT, float* pfINa, float* pfINb, float* pfINc)
{
	checkFileSig("2.waitSig");
	dummyData(DataLen, pfOUT);
}
void _waitFileSig2(int DataLen, float* pfOUT, float* pfINa, float* pfINb, float* pfINc)
{
	string sigName = // std::to_string(((int)*pfINa)) + "-" + 
		"2.waitSig";
	//logF << "call waitFileSig ... " + sigName << endl;

	bool exist = waitMap[sigName];
	if (!exist) {
		*getLogF() << "wait ... " + sigName << endl;
		waitFilePresent2(&logF);
		string path = "N:\\tdx\\FileSig\\" + sigName;
		if ((exists_path(path))) {
			waitMap[sigName] = true;
		}
	}

	dummyData(DataLen, pfOUT);
}


void waitFileSigN(int DataLen, float* pfOUT, float* pfINa, float* pfINb, float* pfINc)
{

	if (!watchFileThreadInited) {
		*getLogF() << "start watchFileThread" << endl;
		//		thread 	th(&waitFilePresent, &logF);
	}

	string sigName = // std::to_string(((int)*pfINa)) + "-" + 
		std::to_string(((int)*pfINb)) + "-" + std::to_string(((int)*pfINc)) + ".waitSig";
	//logF << "call waitFileSig ... " + sigName << endl;

	bool exist = waitMap[sigName];
	if (!exist) {
		*getLogF() << "wait ... " + sigName << endl;
	}

	float ret = exist ? 1 : 0;
	sameData(DataLen, pfOUT, ret);
}


void filterBlockCodes(int DataLen, float* pfOUT, float* pfINa, float* pfINb, float* pfINc)
{

	if (!blockInfoInited) {
		*getLogF() << "blckInfoInit..." << endl;
		Json::Reader blkInfoReader;
		string blockInfo = get_file_contents("D:\\z_my\\data.cal\\tdxData\\blockInfo\\blockInfo.json");
		if (blkInfoReader.parse(blockInfo, blkInfoRoot)) {
			blockInfoInited = true;
		}
	}
	Json::Reader envReader;
	Json::Value envRoot;

	string env = get_file_contents("N:\\tdx\\env.json");
	string curBlk;
	if (envReader.parse(env, envRoot)) {
		curBlk = envRoot["curBlk"].asString();
		//		*getLogF() << "curBlk:" + curBlk << endl;
	}

	string code = padLeft(std::to_string(((int)*pfINa)), 6);

	Json::Value arrValue = blkInfoRoot[code];
	bool inBlock = false;
	if (arrValue != NULL) {
		int size = arrValue.size();
		for (int i = 0; i < size; ++i) {
			string blocks = arrValue[i].asString();
			if (blocks == curBlk) {
				inBlock = true;
			}
		}
	}

	float out = inBlock ? 1 : 0;
	for (int i = 0;i < DataLen;i++)
		pfOUT[i] = out;
}


void notifySigEndA(int DataLen, float* pfOUT, float* pfINa, float* pfINb, float* pfINc)
{
	string code = padLeft(std::to_string(((int)*pfINa)), 6);
	if ("999999" == code) {
		string sigName = // std::to_string(((int)*pfINa)) + "-" +
			std::to_string(CurrentFunType) + ".tdxNotify";
		string path = "N:\\tdx\\Notify\\" + sigName;
		if (!exists_path(path)) {
			string timeS = getTimeStr();
			*getLogF() << timeS << endl;
		}
		string sigName2 = "wait.Sig";
		string path2 = "N:\\tdx\\FileSig\\" + sigName2;
		if (exists_path(path2)) {
			const char* asCharStar = path2.c_str();
			int   flag = std::remove(asCharStar);
		}

		waitMap[sigName2] = false;

		// release file
		if (dataFile.is_open()) {
			dataFile.close();
		}
	}
	dummyDataTrue(DataLen, pfOUT);
}

void notifySigEnd(int DataLen, float* pfOUT, float* pfINa, float* pfINb, float* pfINc)
{
	string code = padLeft(std::to_string(((int)*pfINa)), 6);

	if ("999999" == code) {
		string sigName = // std::to_string(((int)*pfINa)) + "-" +
			std::to_string(((int)*pfINb)) + "-" + std::to_string(((int)*pfINc)) + ".tdxNotify";
		string path = "N:\\tdx\\Notify\\" + sigName;
		if (!exists_path(path)) {
			string timeS = getTimeStr();
			*getLogF() << timeS << endl;
		}
		string sigName2 = std::to_string(((int)*pfINb)) + "-" + std::to_string(((int)*pfINc)) + ".waitSig";
		string path2 = "N:\\tdx\\FileSig\\" + sigName2;
		if (exists_path(path2)) {
			const char* asCharStar = path2.c_str();
			int   flag = std::remove(asCharStar);
		}

		waitMap[sigName2] = false;

		// release file
		if (dataFile.is_open()) {
			dataFile.close();
		}

	}
	dummyData(DataLen, pfOUT);
}


void notifySig(int DataLen, float* pfOUT, float* pfINa, float* pfINb, float* pfINc)
{

	string sigName = std::to_string(((int)*pfINa)) + "-" + std::to_string(((int)*pfINb)) + "-" + std::to_string(((int)*pfINc)) + ".tdxNotify";
	string path = "N:\\tdx\\Notify\\" + sigName;
	if (!exists_path(path)) {
		string timeS = getTimeStr();
		*getLogF() << timeS << endl;
	}
	dummyData(DataLen, pfOUT);
}

void outSigData(int DataLen, float* pfOUT, float* pfINa, float* pfINb, float* pfINc)
{
	string timeS = getTimeStr();
	std::ofstream dataF;
	string sig = std::to_string(((int)*pfINa));
	dataF.open("N:\\tdx\\Notify\\" + sig + ".sig", ios_base::app);
	string code = std::to_string(((int)*pfINb));
	dataF << timeS << "," << padLeft(code, 6) << "," << *pfINc << endl;
	dataF.close();
	dummyData(DataLen, pfOUT);
}


void outWithoutCond(int DataLen, float* pfOUT, float* pfINa, float* pfINb, float* pfINc)
{
	string timeS = getTimeStr();
	if (!dataFile.is_open()) {
		dataFile.open("N:\\tdxSignalLog." + getDateStr() + ".txt", ios_base::app);
	}
	string code = std::to_string(((int)*pfINa));
	dataFile << timeS << " " << DataLen << "," << padLeft(code, 6) << "," << std::to_string(((int)*pfINb)) << "," << *pfINc << endl;
	//	logFile.close();
	dummyData(DataLen, pfOUT);
}




//加载的函数
PluginTCalcFuncInfo g_CalcFuncSets[] =
{
	{1,(pPluginFUNC)&outIfB},
	{2,(pPluginFUNC)&outWithoutCond},
	{3,(pPluginFUNC)&outSigData},
	{4,(pPluginFUNC)&notifySig},
	{5,(pPluginFUNC)&blankFun},
	{6,(pPluginFUNC)&notifySigEnd},
	{7,(pPluginFUNC)&filterBlockCodes},
	{8,(pPluginFUNC)&waitFileSig},
	{81,(pPluginFUNC)&waitFileSig1},
	{82,(pPluginFUNC)&waitFileSig2},
	{89,(pPluginFUNC)&testFun},
	{90,(pPluginFUNC)&waitFile},
	{91,(pPluginFUNC)&CallScreen},
	{92,(pPluginFUNC)&GetCuttentFunType},
	{93,(pPluginFUNC)&notifySigEndA},
	{0,NULL},
};

//导出给TCalc的注册函数
BOOL RegisterTdxFunc(PluginTCalcFuncInfo** pFun)
{
	if (*pFun == NULL)
	{
		(*pFun) = g_CalcFuncSets;
		return TRUE;
	}
	return FALSE;
}
